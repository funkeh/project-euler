-- funkeh :: 2014
-- Project Euler 002 :: Lua

-- Initialise variables
local sum, val1, val2, nextVal = 0, 1, 1, 2

-- Iterate through all values of Fibonacci sequence below 4,000,000
while (nextVal < 4000000) do

    -- Add value to the sum
    sum = sum + nextVal

    -- Find next even number
    for i=1, 3 do
        val1 = val2
        val2 = nextVal
        nextVal = val1 + val2
    end

end

-- Print result
print("Sum of all even values in the Fibonacci sequence below 4,000,000: " ..
        sum)
