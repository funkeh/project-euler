# Project Euler Solutions

I will use this repo to post my own Project Euler solutions. The languages I will be using are:

* C
* Haskell
* JavaScript
* Lua
* Python

## Running codes:

* C:
    * "gcc -o test xxx/solution.c && ./test"
* F#:
    * "fsi --exec xxx/solution.fsx"
* Haskell:
    * "ghci xxx/solution.hs" then type 'main'
* JavaScript:
    * "node xxx/solution.js"
* Lua:
    * "lua xxx/solution.lua"
* Python:
    * "python xxx/solution.py"
    