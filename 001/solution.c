/* funkeh :: 2014
 * Project Euler 001 :: C
 */

#include <stdio.h>

int main() {

    /* Sum of all natural numbers below 1000 */
    int sum = 0;

    /* Variable for iteration */
    int i;

    /* Iterate through all natural numbers below 1000 */
    for (i = 0; i < 1000; i++) {

        /* If number is divisible by 3 or 5 */
        if (i % 3 == 0 || i % 5 == 0) {

            /* Add number to sum */
            sum += i;

        }

    }

    /* Print result */
    printf("Sum of all multiples of 3 or 5 below 1000: %d", sum);

    return 0;

}
