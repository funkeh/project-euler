-- funkeh :: 2014
-- Project Euler 001 :: Haskell

-- Imports useful data functions (union)
import Data.List

-- Print sum of natural numbers divisible by 3 and 5, smaller than 1000
main = do
    let x = "Sum of all multiples of 3 or 5 below 1000: "
    let y = x ++ show(sum(union [3,6..999] [5,10..999]))
    putStrLn y
