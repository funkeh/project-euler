-- funkeh :: 2014
-- Project Euler 001 :: Lua

-- Sum of all natural numbers below 1000
local sum = 0

-- Iterate through all natural numbers below 1000
for i = 0, 999 do

    -- If number is divisible by 3 or 5
    if (i % 3 == 0 or i % 5 == 0) then

        -- Add number to sum
        sum = sum + i

    end

end

-- Print result
print("Sum of all multiples of 3 or 5 below 1000: " .. sum)
