# funkeh :: 2014
# Project Euler 001 :: Python

# Sum of all natural numbers below 1000
sum = 0

# Iterate through all natural numbers below 1000
for i in range(1, 1000):

    # If number is divisible by 3 or 5
    if (i % 3 == 0 or i % 5 == 0):

        # Add number to sum
        sum += i

# Print result
print("Sum of all multiples of 3 or 5 below 1000: " + str(sum))
