// funkeh :: 2014
// Project Euler 001 :: JavaScript
// jshint devel: true

// Sum of all natural numbers below 1000
var sum = 0;

// Iterate through all natural numbers below 1000
for (var i = 0; i < 1000; i++) {

    // If number is divisible by 3 or 5
    if (i % 3 === 0 || i % 5 === 0) {

        // Add number to sum
        sum += i;

    }

}

// Print result
console.log("Sum of all multiples of 3 or 5 below 1000: " + sum);
