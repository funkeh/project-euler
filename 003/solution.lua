-- chikun :: 2014
-- Project Euler 003 :: Lua


number = 600851475143

function primeFactors(n)

    factors = { }

    d = 2

    while n > 1 do

        while n % d == 0 do

            table.insert(factors, d)

            n = n / d

        end

        d = d + 1

    end

    return factors

end

pfs = primeFactors(number)

print(pfs[#pfs])
