-- chikun :: 2014
-- Project Euler 006 :: Lua


function addSum(maxNum, square)

    returnValue = 0

    multiple = 1

    for i = maxNum, 1, -1 do

        if square then

            multiple = maxNum

        end

        returnValue = returnValue + maxNum * multiple

        maxNum = maxNum - 1

    end

    return returnValue

end

sum = addSum(100, true)

power = math.pow(addSum(100), 2)

print(power - sum)
