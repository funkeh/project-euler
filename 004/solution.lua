-- chikun :: 2014
-- Project Euler 004 :: Lua


-- Maximum number
maxNum = 10001

for num1 = 100, 999 do

    for num2 = 100, 999 do

        test = tostring(num1 * num2)

        if test == test:reverse() and
            (num1 * num2) > maxNum then

            maxNum = num1 * num2

        end

    end

end

print(maxNum)
